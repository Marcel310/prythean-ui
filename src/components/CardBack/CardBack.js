import React from 'react';

import styles from './CardBack.module.scss';

import card_back from './../../res/card_back.png';

const CardBack = () => {
    return(
        <div className={styles.CardBack}>
            <img className={styles.CardBackImage} src={card_back} alt="Back of card"></img>
        </div>
    );
}

export default CardBack;