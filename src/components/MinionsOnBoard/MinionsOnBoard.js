import React from 'react';
import { PropTypes } from 'prop-types';

import styles from './MinionsOnBoard.module.scss';

const MinionsOnBoard = ({children}) => {
    return(
        <div className={styles.MinionsOnBoard}>
            { children }
        </div>
    );
}

MinionsOnBoard.propTypes = {
    children: PropTypes.any.isRequired,
}

export default MinionsOnBoard;