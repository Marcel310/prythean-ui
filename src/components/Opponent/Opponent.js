import React from 'react';
import { PropTypes } from 'prop-types';

import TargetableHero from 'containers/TargetableHero/TargetableHero';
import OpponentHand from 'components/OpponentHand/OpponentHand';
import BoardSide from 'components/BoardSide/BoardSide';
import MinionsOnBoard from 'components/MinionsOnBoard/MinionsOnBoard';
import TargetableMinion from 'containers/TargetableMinion/TargetableMinion';

import OpponentModel from 'models/Opponent';

import styles from './Opponent.module.scss';

const Opponent = ({opponent, callbacks}) => {
    const minions = opponent.activeMinions.map((minion, index) => (
        <TargetableMinion
            minion={minion}
            isDraggable={false}
            key={index}
            callbacks={callbacks}
        />
    ));

    return(
        <div className={styles.Opponent}>
            <div className={styles.OpponentHandWrapper}>
                <OpponentHand handCount={opponent.handCardCount} />
            </div>
            <div className={styles.OpponentHero}>
                <TargetableHero hero={opponent.hero} />
            </div>
            <BoardSide>
                <MinionsOnBoard>
                    { minions }
                </MinionsOnBoard>
            </BoardSide>
        </div>
    );
}

Opponent.propTypes = {
    opponent: PropTypes.instanceOf(OpponentModel).isRequired,
}

export default Opponent;