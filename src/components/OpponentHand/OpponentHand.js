import React from 'react';
import { PropTypes } from 'prop-types';

import CardBack from 'components/CardBack/CardBack';

import styles from './OpponentHand.module.scss';

const OpponentHand = ({handCount}) => {
    let cardList = [];

    for (let i = 0; i < handCount; i++) {
        cardList.push(
            <CardBack key={i} />
        )
    }

    return (
        <div className={styles.OpponentHand}>
            { cardList }
        </div>
    )
};

OpponentHand.propTypes = {
    handCount: PropTypes.number.isRequired,
}

export default OpponentHand;