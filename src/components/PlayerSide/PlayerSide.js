import React from 'react';
import { PropTypes } from 'prop-types';

import styles from './PlayerSide.module.scss';

const PlayerSide = ({children}) => (
    <div className={styles.PlayerSide}>
        { children }
    </div>
);

PlayerSide.propTypes = {
    children: PropTypes.any.isRequired,
}

export default PlayerSide;