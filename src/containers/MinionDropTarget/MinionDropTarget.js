import React, { Component } from 'react';
import { DropTarget } from 'react-dnd';

import DraggableMinion from 'containers/DraggableMinion/DraggableMinion';

class MinionDropTarget extends Component {
    render() {
        const { connectDropTarget, minion, isPlayersTurn } = this.props;

        return connectDropTarget(
            <div>
                <DraggableMinion minion={minion} isPlayersTurn={isPlayersTurn} />
            </div>
        );
    }
}

const cardDropTarget = {
    canDrop(props) {
        return props.isPlayersTurn;
    },
    drop(props, monitor, component) {
        console.log("Dropping card");
    }
}

function collect(connect) {
    return {
        connectDropTarget: connect.dropTarget(),
    };
}

export default DropTarget('CARD', cardDropTarget, collect)(MinionDropTarget);