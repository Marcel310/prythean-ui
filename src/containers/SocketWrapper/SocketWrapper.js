import React, { Component } from 'react';
import GameBoard from './../../components/GameBoard/GameBoard';

import GameModel from 'models/Game';
import OpponentModel from 'models/Opponent';

const URL = 'ws://localhost:8080/ws/game/A';

class SocketWrapper extends Component {

    constructor(props) {
        super(props);

        // Initialise state
        this.state = {
            game: null,
        };

        this.ws = new WebSocket(URL);

        // Connection created
        this.ws.onopen = () => {
            console.log("Connected");
        }

        // Message received
        this.ws.onmessage = (event) => {
            console.log("Message received");

            const jsonGameData = JSON.parse(event.data);

            this.setState({
                game: new GameModel(
                    jsonGameData.player,
                    new OpponentModel(
                        jsonGameData.enemyHandCount,
                        jsonGameData.enemyMinions,
                        jsonGameData.enemyHero,
                    ),
                    jsonGameData.turnCount,
                    jsonGameData.isPlayersTurn,
                ),
            });

            console.log(this.state.game);
        }

        this.callbacks = {
            endTurnButton: () => {
                this.ws.send(JSON.stringify({eventTitle: "END_TURN_BUTTON_PRESSED"}));
            },
            playCard: (cardHandIndex) => {
                console.log(`Playing card with index: ${cardHandIndex}`);
                this.ws.send(JSON.stringify({
                    eventTitle: "PLAY_HAND_CARD",
                    cardOneIndex: cardHandIndex,
                }));
            },
            attack: (minionIndex, targetIndex) => {
                this.ws.send(JSON.stringify({
                    eventTitle: "MINION_ATTACK_TARGET",
                    cardOneIndex: minionIndex,
                    targetIndex: targetIndex,
                }));
            },
        }

    }

    render() {
        // Only render if the server has already sent the game state
        if (this.state.game === null) {
            return null;
        }

        return <GameBoard game={this.state.game} callbacks={this.callbacks} />
    }
}

export default SocketWrapper;
