import React, { Component } from 'react';
import { DropTarget } from 'react-dnd';

import Hero from 'components/Hero/Hero';

class TargetableHero extends Component {
    render() {
        const { connectDropTarget, hero } = this.props;

        return connectDropTarget(
            <div>
                <Hero hero={hero} />
            </div>
        );
    }
}

const heroDropTarget = {
    drop(props, monitor, component) {

        const indexAttacker = monitor.getItem().card.details.index;
       console.log(props);
        const indexSelf = -1;
        props.callbacks.attack(indexAttacker, indexSelf);
        console.log("Hero attacked by: " + "lol");
    }
}

function collect(connect) {
    return { connectDropTarget: connect.dropTarget() };
}

export default DropTarget('MINION', heroDropTarget, collect)(TargetableHero);