import React, { Component } from 'react';
import { DropTarget } from 'react-dnd';

import Minion from 'components/Minion/Minion';

class TargetableMinion extends Component {
    render() {
        const { connectDropTarget, minion } = this.props;

        return connectDropTarget(
            <div style={{margin: '0 10px'}}>
                <Minion minion={minion} />
            </div>
        );
    }
}

const minionDropTarget = {
    drop(props, monitor, component) {
        const indexAttacker = monitor.getItem().card.details.index;
        const indexSelf = props.minion.details.index;
        console.log(props);
        console.log("Minion dropped on other minion");
        props.callbacks.attack(indexAttacker, indexSelf);
    },
};

function collect(connect) {
    return { connectDropTarget: connect.dropTarget() };
}

export default DropTarget('MINION', minionDropTarget, collect)(TargetableMinion);