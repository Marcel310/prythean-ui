class MinionModel {
    constructor(cardInfo, exhausted) {
        this.cardInfo = cardInfo;
        this.exhausted = exhausted;
    }
}

export default MinionModel;