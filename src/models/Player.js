class PlayerModel {
    constructor(handCards, activeMinions, hero) {
        this.handCards = handCards;
        this.activeMinions = activeMinions;
        this.hero = hero;
    }
}

export default PlayerModel;